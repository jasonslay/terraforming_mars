from player import Player
from game import Game


def test_produce():
    p = Player(
        "test_player",
        tr=20,
        money=0,
        money_production=1,
        heat=10,
        heat_production=1,
        energy=5,
        energy_production=3,
    )
    p.produce()
    assert p.tr == 20
    assert p.money == 20 + 1
    assert p.heat == 10 + 1 + 5
    assert p.energy == 3


def test_generation_count():
    g = Game([], generation=1)
    g.produce()
    assert g.generation == 2
