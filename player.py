"""The Player object."""
from dataclasses import dataclass


@dataclass
class Player:
    """Player object."""

    name: str
    tr: int = 20
    money: int = 0
    money_production: int = 1
    steel: int = 0
    steel_production: int = 1
    titanium: int = 0
    titanium_production: int = 1
    plants: int = 0
    plant_production: int = 1
    energy: int = 0
    energy_production: int = 1
    heat: int = 0
    heat_production: int = 1

    def produce(self):
        """Perform the production portion of the round."""
        self.heat += self.energy
        self.energy = 0
        self.money += self.tr + self.money_production
        self.steel += self.steel_production
        self.titanium += self.titanium_production
        self.plants += self.plant_production
        self.energy += self.energy_production
        self.heat += self.heat_production

    @property
    def summary(self):
        """Return the summary of the Player."""
        return (
            f"{self.name} ({self.tr})\n"
            + f"Money: {self.money} [{self.money_production}]\n"
            + f"Steel: {self.steel} [{self.steel_production}]\n"
            + f"Titanium: {self.titanium} [{self.titanium_production}]\n"
            + f"Plants: {self.plants} [{self.plant_production}]\n"
            + f"Energy: {self.energy} [{self.energy_production}]\n"
            + f"Heat: {self.heat} [{self.heat_production}]"
        )

    def summarize(self):
        """Print the summary of the Player."""
        print(self.summary)
