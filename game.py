from dataclasses import dataclass


@dataclass
class Game:
    "Game object."

    players: list
    generation: int = 1

    def produce(self):
        """Produce for all players."""
        self.generation += 1
        for player in self.players:
            player.produce()

    @property
    def summary(self):
        """Return summary string."""
        result = f"Generation: {self.generation}"
        for player in self.players:
            result += "\n\n" + player.summary
        return result

    def summarize(self):
        """Print summary."""
        print(self.summary)
